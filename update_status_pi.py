#! /usr/bin/env python3

# Idea : Matt Hawkins
# http://www.raspberrypi-spy.co.uk/2012/08/reading-analogue-sensors-with-one-gpio-pin/
# make the GPIOs work under python3 : sudo apt-get install python3-rpi.gpio 

SENSOR_PIN = 2
RED_PIN = 4
GREEN_PIN = 17

LIGHT_THRESHOLD = (0.000576 + 0.000957) / 2

SFTP_SERVER = '192.168.0.60'
SFTP_USER = 'daan'
SFTP_PORT = '22'
SFTP_ID_FILE = '/home/pi/.ssh/id_rsa'

from datetime import datetime, timedelta
import RPi.GPIO as GPIO, time
import os
from dateutil import tz
tzinfo = tz.gettz('Europe/Paris')
import subprocess
import time

# Tell the GPIO library to use
# Broadcom GPIO references
try:
  GPIO.setmode(GPIO.BCM)
except:
  pass

GPIO.setup(RED_PIN, GPIO.OUT)
GPIO.setup(GREEN_PIN, GPIO.OUT)


# Define function to measure charge time
def RC_time (PiPin):
  # Discharge capacitor
  GPIO.setup(PiPin, GPIO.OUT)
  GPIO.output(PiPin, GPIO.LOW)
  time.sleep(0.2)

  GPIO.setup(PiPin, GPIO.IN)
  # Count loops until voltage across
  # capacitor reads high on GPIO

  before = time.time()
  while (GPIO.input(PiPin) == GPIO.LOW):
    pass
  return time.time() - before


def avg_RC_time(PiPin, iter):
	sum = 0
	for i in range(iter):
		sum = sum + RC_time(PiPin)
	return sum/iter

# Main program loop
light_timing = avg_RC_time(2,20)

active = light_timing < LIGHT_THRESHOLD
if active:
    GPIO.output(RED_PIN, GPIO.LOW)
    GPIO.output(GREEN_PIN, GPIO.HIGH)
else:
    GPIO.output(GREEN_PIN, GPIO.LOW)
    GPIO.output(RED_PIN, GPIO.HIGH)


now = datetime.now()
now = now.replace(tzinfo=tzinfo)
s = '{:>6.2f}; {}'.format(light_timing, now)
# print(s) 
with open(os.path.join(os.environ['HOME'], 'light_log.csv'), 'a') as f:
    f.write(s+'\n')
heartbeat = '{}; {}'.format('open' if active else 'closed', now)
with open(os.path.join(os.environ['HOME'], 'heartbeat.csv'), 'w') as f:
    f.write(heartbeat+'\n')

if new_state == 'open':
    subprocess.call(UPDATE_COMMAND_OPEN.split()+[str(now)])
else:
    subprocess.call(UPDATE_COMMAND_CLOSE.split()+[str(now)])

