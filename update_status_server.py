#! /usr/bin/env python3

import json
import iso8601
from datetime import datetime, timedelta
from dateutil import tz
tzinfo = tz.gettz('Europe/Paris')

# file in which the current state and timestamp will be written
HEARTBEAT_FILE = 'heartbeat.csv'

# file into which the complete json will be read from and dumped into.
JSON_FILE = 'spaceapi.json'

# time (in minutes) after which the heartbeat is to be considered stale
HEARTBEAT_TIMEOUT = 10

with open(HEARTBEAT_FILE, 'r') as f:
    lines = f.readlines()
assert len(lines) == 1
new_status, timestamp = lines[0].strip().split(r';')
new_status = new_status.strip()
timestamp = iso8601.parse_date(timestamp.strip(), default_timezone=tzinfo)
now = datetime.now().replace(tzinfo=tzinfo)

# if the last heartbeat is too old we acknowledg that we know nothing
if now - timestamp > timedelta(minutes=HEARTBEAT_TIMEOUT) or new_status not in ['open', 'closed']:
    # spaceapi defines a json null (corresponds to a python None) as "undefined"
    # see http://spaceapi.net/documentation#documentation-ref-13-root-state-open
    new_status = None 
else:
    new_status = new_status == 'open'

with open(JSON_FILE, 'r') as f:
    s = json.load(f)
s['state']['open'] = new_status
with open(JSON_FILE, 'w') as f:
    json.dump(s, f)
