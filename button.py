#!/usr/bin/env python3

# script by Alex Eames http://RasPi.tv/  
# http://raspi.tv/2013/how-to-use-interrupts-with-python-on-the-raspberry-pi-and-rpi-gpio  

import time
import RPi.GPIO as gpio  
import os
import subprocess
from datetime import datetime
import argparse
import numpy as np
from threading import Timer
from enum import IntEnum
import requests
import json
import threading

class LOGalState(IntEnum):
    closed = 0
    closing = 1
    opened = 2
    opening = 3

class InputSignal(IntEnum):
    light_on = 0
    light_off = 1
    button_open = 2
    button_close = 3
    opening_timeout = 5


gpio.setmode(gpio.BCM)  

ENDPOINT = 'https://www.logre.eu/spaceapi.json'

LED_TIMEOUT = 5.0 #5*60
BLINK_FREQUENCY = 0.5
DEBOUNCE_DELAY = 0.25

PIN_LDR = 4
DISCHARGE_TIME = 0.01

RED_PIN = 25
GREEN_PIN = 8
WHITE_PIN = 7

BUTTON_ON_PIN  = 24
BUTTON_OFF_PIN = 23

default_sensor_interval = 1
default_light_detection_measures = 3
default_light_threshold = 1e-3
default_log_interval = 60

STATUS_SYMBOLS = '-\|/'

# command to execute for updatin
UPDATE_COMMAND_OPEN = 'ssh spaceapi@logre.eu open'
UPDATE_COMMAND_CLOSE = 'ssh spaceapi@logre.eu close'

# start in "closed" mode TODO call this LOGstate or such
last_change = time.time()

last_light_change = last_change
  
gpio.setwarnings(False)
gpio.setup(RED_PIN, gpio.OUT)  
gpio.setup(GREEN_PIN, gpio.OUT)  
gpio.setup(WHITE_PIN, gpio.OUT)  
gpio.output(RED_PIN, gpio.LOW)
gpio.output(GREEN_PIN, gpio.LOW)
gpio.output(WHITE_PIN, gpio.LOW)

# set pins as input.  Pull them up to stop false signals  
gpio.setup(BUTTON_ON_PIN, gpio.IN, pull_up_down=gpio.PUD_UP)  
gpio.setup(BUTTON_OFF_PIN, gpio.IN, pull_up_down=gpio.PUD_UP)  


def on_light_changed(new_light_state):
    global light_state
    light_state = new_light_state

    if not new_light_state:
        print('light switched off ')
        set_state(LOGalState.closing)
    else:
        print('light switched on ')
        set_state(LOGalState.opening)


def timer_timeout():
    # since we are very cautious we only ever *automatically* set the state to "closed"
    # it is the responsibility of the other events to disable the timeout or set the state to "opened"
    print('no user action. timing out')
    set_state(LOGalState.closed)


def get_server_state(timeout=2):
    try:
        r = requests.get(ENDPOINT, timeout=timeout)
        data = r.json()
        server_state = data['state']['open']
        return LOGalState.opened if server_state else LOGalState.closed
    except Exception as e:
        print('getting server state failed: {}'.format(e))
        print('defaulting to "closed"')  # TODO: is this really a good idea?
        return LOGalState.closed


def set_state(new_state, channel=None):
    global last_change
    global state
    global light_state
    global update_server
    global change_timeout
    now = time.time()
    if now - last_change < DEBOUNCE_DELAY:
        # print('ignoring fast state change')
        return
    
    if channel is not None:
        print('Detected button push on {}'.format(channel))
    last_change = now

    # we got some new input. that means the old timeout is useless.
    if change_timeout is not None:
        change_timeout.cancel()

    old_state = state
    state = new_state

    if state == LOGalState.opened:
        gpio.output(RED_PIN, gpio.LOW)
        gpio.output(GREEN_PIN, gpio.HIGH)
        gpio.output(WHITE_PIN, gpio.LOW)
        if update_server:
            subprocess.call(UPDATE_COMMAND_OPEN.split())
            print('sending open() to server')
    elif state == LOGalState.closed:
        gpio.output(RED_PIN, gpio.HIGH)
        gpio.output(GREEN_PIN, gpio.LOW)
        gpio.output(WHITE_PIN, gpio.LOW)
        if update_server:
            print('sending close() to server')
            subprocess.call(UPDATE_COMMAND_CLOSE.split())
    elif state == LOGalState.opening:
        if old_state == LOGalState.opened:
            print('LOGal already opened. no need for a fuzz')
            return
        else:
            print('LOGal might open. starting to notify peeps by annoying blinking protocol.')
            blink_thread.run()
            change_timeout = threading.Timer(LED_TIMEOUT, timer_timeout)
            change_timeout.start()
    else:
        if old_state == LOGalState.closed:
            print('LOGal is already closed, but state is closing, likely because of light change. shining light anyway')
        print('starting closing timeout --> switch white light on')
        change_timeout = threading.Timer(LED_TIMEOUT, timer_timeout)
        change_timeout.start()
        gpio.output(WHITE_PIN, gpio.HIGH)


class BlinkThread(threading.Thread):
    def run(self):
        global state
        ledout = True
        while state == LOGalState.opening:
            gpio.output(WHITE_PIN, gpio.HIGH if ledout else gpio.LOW)
            ledout = not ledout
            time.sleep(BLINK_FREQUENCY)
blink_thread = BlinkThread()

gpio.add_event_detect(BUTTON_ON_PIN, gpio.RISING, callback=lambda channel: set_state(LOGalState.opened, channel))  
gpio.add_event_detect(BUTTON_OFF_PIN, gpio.RISING, callback=lambda channel: set_state(LOGalState.closed, channel))  
change_timeout = None

def RC_time(pin, timeout=1):
  # Discharge capacitor
  gpio.setup(pin, gpio.OUT)
  gpio.output(pin, gpio.LOW)
  time.sleep(DISCHARGE_TIME)
  before = time.time()

  # TODO: this would be much better I think
  # gpio.setup(pin, gpio.IN)
  # gpio.wait_for_edge(pin, gpio.RISING)

  # Count loops until voltage across
  # capacitor reads high on gpio
  gpio.setup(pin, gpio.IN, pull_up_down=gpio.PUD_OFF)
  while gpio.input(pin) == gpio.LOW and time.time() - before < timeout:
      pass

  return time.time() - before


def avg_RC_time(pin, iter):
    if args.fake_measurements:
        t = int(time.time())
        measurements = np.random.rand(iter)
        if (t//20) % 2 == 0:
            measurements *= 0.9 * args.light_threshold
        else:
            measurements *= 1 - args.light_threshold
            measurements += 1.1 * args.light_threshold
        return measurements.mean(), measurements
    measures = []
    sum = 0
    for i in range(iter):
        m_t = RC_time(pin)
        measures.append(m_t)
        sum = sum + m_t
    return sum/iter, measures

def positive_float(o):
    i = float(o)
    if i < 0:
        raise ValueError('Input must be positive! Got {}'.format(o))
    return i

def non_negative_int(o):
    i = int(o)
    if i < 0:
        raise ValueError('Input must be non-negative! Got {}'.format(o))
    return i

def LOGstate_tostring(s):
    if s == LOGalState.opened:
        return 'opened'
    elif s == LOGalState.opening:
        return 'opening'
    elif s == LOGalState.closing:
        return 'closing'
    else:
        return 'closed'


def main():

    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-f', '--fake-measurements', action='store_true',
                        help='fake measurements for debugging')
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='print all measurements, not only the average')
    parser.add_argument('-l', '--local', dest='update_server',
                        action='store_false', help='do not send status changes to server')
    parser.add_argument('-n', '--no-logging', dest='write_logfile',
                        action='store_false', help='disable output to logfile')
    parser.add_argument('--ldr-pin', type=non_negative_int, default=PIN_LDR,
                        help='pin on which the LDR is connected')
    parser.add_argument('-i', '--iterations', type=non_negative_int, default=20,
                        help='number of measures to average over')
    parser.add_argument('-m', '--measurements', default=0, type=non_negative_int,
                        help='number of measurements to take. 0 for unlimited')
    parser.add_argument('-s', '--sensor-interval', default=default_sensor_interval, type=positive_float,
                        help='time to sleep between measurements')
    parser.add_argument('-t', '--light-threshold', default=default_light_threshold, type=positive_float,
                        help='threshold for the charge times. if the average charge time over <light-detection-measures> '
                             'goes above this value the light is considered off.')
    parser.add_argument('--light-detection-measures', default=default_light_detection_measures, type=positive_float,
                        help='number of measurements to use for light change detection')
    parser.add_argument('--log-interval', default=default_log_interval, type=int,
                        help='interval at which to write into logfile. <=0 to log on every measurement')
    parser.add_argument('--logfile', default=os.path.join(os.environ['HOME'], 'light_log.csv'),
                        help='file to log the measurements to')
    global args
    args = parser.parse_args()
    args.log_interval = max(args.sensor_interval, args.log_interval)
    print(args)
    global update_server
    global state
    update_server = args.update_server

    try:
        state = get_server_state()
        print('State retrieved from server: {}'.format(state))
        set_state(state)
        i = 0
        last_log = time.time()
        avg, _ = avg_RC_time(args.ldr_pin, 1)
        light_state = avg < args.light_threshold
        last_light_values = avg * np.ones(args.light_detection_measures, dtype='float')
        while args.measurements == 0 or i < args.measurements:
            avg, measures = avg_RC_time(args.ldr_pin, args.iterations)

            mean = last_light_values.mean()
            current_light = mean <= args.light_threshold
            # averaging over the actual times is a bad idea since there are
            # some orders of magnitude between "light on" and "light off"
            last_light_values[i % args.light_detection_measures] = avg

            if current_light != light_state:
                on_light_changed(current_light)
            light_state = current_light

            # do the logging
            measures = ['{:12.04}'.format(f) for f in measures]
            now = time.time()
            now_dt = datetime.fromtimestamp(now)

            statestring = LOGstate_tostring(state)
            s = '{}; {:>12.04e}; {}; {}'.format(now_dt, avg, statestring, '; '.join(measures))
            if args.write_logfile and now-last_log > args.log_interval:
                with open(args.logfile, 'a') as f:
                    f.write(s+'\n')
                last_log = now

            # show current state
            if args.verbose:
                print('{}  {}'.format(s, STATUS_SYMBOLS[i%len(STATUS_SYMBOLS)]),
                      end='\r', flush=True)
            else:
                print('{}  {:>12.04e}  {}  {}'.format(now_dt, avg, statestring, STATUS_SYMBOLS[i%len(STATUS_SYMBOLS)]),
                      end='\r', flush=True)
            time.sleep(args.sensor_interval)
            i += 1
    except KeyboardInterrupt:  
        print('ImpatientUserException!')
    finally:
        if change_timeout is not None:
            change_timeout.cancel()

        gpio.output(RED_PIN, gpio.LOW)
        gpio.output(GREEN_PIN, gpio.LOW)
        gpio.output(WHITE_PIN, gpio.LOW)
        gpio.cleanup()           # clean up gpio on normal exit 

if __name__ == '__main__':
    main()

